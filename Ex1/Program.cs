﻿using System;
using System.Collections.Generic;

namespace Ex1
{
  class Program
  {
    static void Main(string[] args)
    {
      var strings = new List<string>
      {
        "Hello",
        "Abuse",
        "simple project",
        "R2D2",
        "simple",
        "Abstraction",
        "Agregate",
        "defuse",
        "array_of_string",
        "778920",
        "gas@lamp.com",
        "relationships",
        "baseRule",
        "my opinion",
        "asl_bust2@relay@a.com"
      };

      var stringsWithNumber = strings.Only(StringType.WithNumber);      
      Console.WriteLine($"Strings {StringType.WithNumber:F}: {string.Join(',', stringsWithNumber)}");

      var stringsLongerThan10 = strings.Only(StringType.LongerThan10);
      Console.WriteLine($"Strings {StringType.LongerThan10:F}: {string.Join(',', stringsLongerThan10)}");

      var stringsSmallerThan10 = strings.Only(StringType.SmallerThan10);
      Console.WriteLine($"Strings {StringType.SmallerThan10:F}: {string.Join(',', stringsSmallerThan10)}");

      var stringsEmail = strings.Only(StringType.Email);
      Console.WriteLine($"Strings {StringType.Email:F}: {string.Join(',', stringsEmail)}");

      var stringsWithUppercaseCharacter = strings.Only(StringType.WithUppercaseCharacter);
      Console.WriteLine($"Strings {StringType.WithUppercaseCharacter:F}: {string.Join(',', stringsWithUppercaseCharacter)}");

      var stringsLowercase = strings.Only(StringType.Lowercase);
      Console.WriteLine($"Strings {StringType.Lowercase:F}: {string.Join(',', stringsLowercase)}");

      var stringsWithManyWords = strings.Only(StringType.WithManyWords);
      Console.WriteLine($"Strings {StringType.WithManyWords:F}: {string.Join(',', stringsWithManyWords)}");

      var stringsWithRepeatingCharacters = strings.Only(StringType.WithRepeatingCharacters);
      Console.WriteLine($"Strings {StringType.WithRepeatingCharacters:F}: {string.Join(',', stringsWithRepeatingCharacters)}");
    }
  }
}
