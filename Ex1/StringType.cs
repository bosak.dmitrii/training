﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ex1
{
  public enum StringType
  {
    WithNumber,
    LongerThan10,
    SmallerThan10,
    Email,
    WithUppercaseCharacter,
    Lowercase,
    WithManyWords,
    WithRepeatingCharacters
  }
}
